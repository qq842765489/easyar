// pages/recognition/recognition.js
import { createScopedThreejs } from 'threejs-miniprogram'

// const { renderCube } = require('../test-cases/cube')
// const { renderCubes } = require('../test-cases/cubes')
// const { renderSphere } = require('../test-cases/sphere')
const { renderModel } = require('../test-cases/model')

const app = getApp()
Page({
  data: {
    height: '360',
    cheight:'0',
    status: false,
    scanStatus: 'none',
    webgls:true,
    msg: "请点击识别图片"
  },

  onLoad: function (options) {
	  //检查是否存在新版本
	  wx.getUpdateManager().onCheckForUpdate(function (res) {
      // 请求完新版本信息的回调
      // console.log("是否有新版本：" + res.hasUpdate);
      if (res.hasUpdate) { //如果有新版本
        // 小程序有新版本，会主动触发下载操作（无需开发者触发）
        wx.getUpdateManager().onUpdateReady(function () { //当新版本下载完成，会进行回调
          wx.showModal({
            title: '更新提示',
            content: '新版本已经准备好，单击确定重启应用',
            showCancel: false,
            success: function (res) {
              if (res.confirm) {
                wx.clearStorageSync()
                // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
                wx.getUpdateManager().applyUpdate();
              }
            }
          })

        })
        // 小程序有新版本，会主动触发下载操作（无需开发者触发）
        wx.getUpdateManager().onUpdateFailed(function () { //当新版本下载失败，会进行回调
          wx.showModal({
            title: '提示',
            content: '检查到有新版本，但下载失败，请检查网络设置',
            showCancel: false,
          })
        })
      }
    });
	
    this.ctx = wx.createCameraContext();
    wx.getSystemInfo({
      success: res => {
        this.setData({ height: res.windowHeight * 0.8 });
      }
   });
    //显示
    
  },

  stopScan: function () {
    this.setData({ scanStatus: 'none' });
  },

  onShow: function () {
    this.setData({ msg: '请点击识别图片' });
  },

  error: function (e) {
    this.setData({ msg: '打开摄像头失败，请点击“立即体验' });
  },

  searchPhoto: function (filePath) {
    console.log(filePath);
    wx.uploadFile({
      url: 'https://3.1415926.com.cn/recognize_zikim.php',
      filePath,
      name: 'image',
      success: res => {
      this.status = false;

    let msg = JSON.parse(res.data);
    console.log(res);
    if (msg.statusCode != 0) {
      this.setData({ msg: '未识别到目标，请点击屏幕继续识别' });
    } else {
      
      this.setData({ msg: '识别成功', webgls: false });
      
      setTimeout(() => {
        console.info('go to webar');
        console.log(msg.result.target.isAR);
        if (msg.result.target.isAR==1){
          wx.redirectTo({
            url: '../show/index'
          })
        }else{
          wx.redirectTo({
            url: '../showimg/index?targetId=' + msg.result.target.targetId
          })
        }
       
    }, 500);
    }
  },
    fail: err => {
      this.status = false;
      this.setData({ msg: JSON.stringify(err) });
    }
  });
  },

  takePhoto: function (e) {
    console.log(111);
    this.setData({webgls: true });
    if (this.status) return;

    this.status = true;

    this.ctx.takePhoto({
      quality: 'normal',
      success: res => {
      this.setData({ msg: '识别中...' });
    this.searchPhoto(res.tempImagePath)
  },
    fail: err => {
      this.stopScan();
      this.setData({ msg: '未识别到目标' });
    }
  });
  }
})