// show/index.js
import { createScopedThreejs } from 'threejs-miniprogram'
const { renderModel } = require('../test-cases/model')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isAR:0,
    targetId:"",
    title:"",
    featrueimage:"",
    content:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.ctx = wx.createCameraContext();
    wx.getSystemInfo({
      success: res => {
        this.setData({ height: res.windowHeight * 0.8 });
      }
    });
    
    wx.createSelectorQuery().select('#webgl').node().exec((res) => {
        const canvas = res[0].node
        const THREE = createScopedThreejs(canvas)
        renderModel(canvas, THREE)
    });
    
   
  },
  returnback: function (e) {
    wx.redirectTo({
      url: '../recognition/recognition',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})