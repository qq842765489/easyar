// show/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isAR:0,
    title:"",
    featrueimage:"",
    content:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.ctx = wx.createCameraContext();
    wx.getSystemInfo({
      success: res => {
        this.setData({ 
          height: res.windowHeight * 0.8
        });
      }
    });
    if (options.targetId =="6d89dc53-8a5b-44ce-b308-44ef1d7c7160"){
      this.setData({
        title: "香港警察主办—香港理工大学音乐节",
        contents: "2019年11月17日夜，香港理工大学音乐节。本歌单主要放送警察叔叔已经放过的金曲。",
        featrueimage: "https://3.1415926.com.cn/SimpleThreeJsExample/featrueimage/6d89dc53-8a5b-44ce-b308-44ef1d7c7160.jpg"
      });
    }else if (options.targetId == "32abbc87-16b6-45dc-a87f-b6cd8e42c34a"){
      this.setData({
        title: "金坷垃是检验神曲的唯一标准",
        contents: "金坷垃有一股神奇的魔力，无论你将世界上哪种曲调配上金坷垃的歌词，你都会发现莫名的带感！就是因为这样，金坷垃才能再战五百年，它不仅仅再是以前的浮夸广告，而是当你听了一首歌，将这首歌脑补金坷垃，你会发现金坷垃才是原版！现在涌现无数调教大神，以黑科技，将音乐用金坷垃的独有方式再现辉煌。拯救了这半死不活的乐坛！民间有一种说法，世界上只有两种音乐，一种叫金坷垃，另一种叫翻唱金坷垃！",
        featrueimage: "https://3.1415926.com.cn/SimpleThreeJsExample/featrueimage/32abbc87-16b6-45dc-a87f-b6cd8e42c34a.jpg"
      });
    } else if (options.targetId == "86461dff-273c-48d7-b602-5c0ef2614b9d") {
      this.setData({
        title: "The Classics (Bonus Track Version)",
        contents: "本张专辑主要注重对经典上海爵士乐的解构、雕琢与重塑，同时亦不乏诸如“何日君再来”等金曲的改编新唱，借助于上海爵士歌手张乐的女声演绎，加之耐人寻味的歌词，给人耳目一新的别样感受。",
        featrueimage: "https://3.1415926.com.cn/SimpleThreeJsExample/featrueimage/86461dff-273c-48d7-b602-5c0ef2614b9d.jpg"
      });
    }
    
  },
  returnback: function (e) {
    wx.redirectTo({
      url: '../recognition/recognition',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    // return {
    //   path: '../showimg/index'
    // }
  }
})